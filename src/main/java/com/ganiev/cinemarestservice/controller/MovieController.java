package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.movie.MovieDto;
import com.ganiev.cinemarestservice.service.movie.MovieServiceImpl;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.DEFAULT_PAGE_SIZE;

@RestController
@RequestMapping("api/movie")
public class MovieController {

    private final MovieServiceImpl movieService;

    public MovieController(MovieServiceImpl movieService) {
        this.movieService = movieService;
    }


    @GetMapping
    public HttpEntity<?> getAllMovies(
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size,
            @RequestParam(name = "page", defaultValue = "1") int page,
            @RequestParam(name = "search", defaultValue = "") String search,
            @RequestParam(name = "sort", defaultValue = "title") String sort,
            @RequestParam(name = "direction", defaultValue = "true") boolean direction
    ) {
        return movieService.getMovies(size, page, search, direction, sort);
    }

    @PostMapping({"", "/{id}"})
    public HttpEntity<?> saveMovie(@RequestPart("file") MultipartFile file,
                                   @RequestPart("dto") MovieDto dto,
                                   @PathVariable(required = false) UUID id) {
        return movieService.saveMovie(id, file, dto);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getSingleMovie(@PathVariable UUID id) {
        return movieService.getMovieById(id);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteMovie(@PathVariable UUID id) {
        return movieService.deleteMovie(id);
    }

}
