package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.movie_session.MovieSessionDto;
import com.ganiev.cinemarestservice.service.movie_session.MovieSessionServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/movie-session")
public class MovieSessionController {

    private final MovieSessionServiceImpl movieSessionService;

    @PostMapping
    public HttpEntity<?> addNewSession(@RequestBody MovieSessionDto dto) {
        return movieSessionService.addNewSession(dto);
    }

    @GetMapping("/{announcementId}")
    public HttpEntity<?> getAnnouncementSessions(@PathVariable UUID announcementId) {
        return movieSessionService.getAnnouncementSessions(announcementId);
    }
}
