package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.service.seat.SeatServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/seat")
public class SeatController {

    private final SeatServiceImpl seatService;

    @GetMapping("/available-seats/{sessionId}")
    public HttpEntity<?> getAvailableSeats(@PathVariable UUID sessionId) {
        return seatService.getAvailableSeats(sessionId);
    }

}
