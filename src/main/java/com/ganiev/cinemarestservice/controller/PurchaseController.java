package com.ganiev.cinemarestservice.controller;

import com.ganiev.cinemarestservice.dto.PurchaseDto;
import com.ganiev.cinemarestservice.service.purchase.PurchaseServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/purchase")
public class PurchaseController {

    private final PurchaseServiceImpl purchaseService;

    @PostMapping
    public HttpEntity<?> purchaseTicket(@RequestBody PurchaseDto dto) {
        return purchaseService.purchaseTicket(dto);
    }
}
