package com.ganiev.cinemarestservice.util;

public interface Constants {
    String SUCCESS_SAVE = "Successfully saved!";
    String FAILED_TO_SAVE = "Failed to save!";
    String EXISTS = "Object already exists!";
    String OBJECT_NOT_FOUND = "Object not found!";
    String SUCCESS_DELETE = "Successfully deleted!";
    String FAILED_TO_DELETE = "Failed to delete!";
    String EMPTY_LIST = "List is empty!";
    String ERROR = "Error!";
    String DEFAULT_PAGE_SIZE = "5";
    Integer ADDITIONAL_MINUTE = 30;
    Double ADDITIONAL_FEE_IN_PERCENT = 5.0;
}
