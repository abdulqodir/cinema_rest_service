package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class PriceCategory extends AbsEntity {

    @Column(nullable = false, unique = true)
    String name;

    Double additionalFeeInPercent;

    String color;

}
