package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Attachment extends AbsEntity {

    @Column(nullable = false)
    String fileName;

    @Column(nullable = false)
    String contentType;

    @Column(nullable = false)
    Long size;

    @OneToOne(cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    AttachmentContent attachmentContent;

    public static Attachment prepareAttachment(MultipartFile file) throws IOException {
        if (file != null && !file.isEmpty()) {
            String[] split = Objects.requireNonNull(file.getOriginalFilename()).split("\\.");
            if (2 > split.length) {
                return null;
            }
            String generatedFileName = "IMG-" + LocalDate.now() + "-" + LocalTime.now();
            return new Attachment(
                    generatedFileName,
                    file.getContentType(),
                    file.getSize(),
                    new AttachmentContent(file.getBytes()));
        }
        return null;
    }
}
