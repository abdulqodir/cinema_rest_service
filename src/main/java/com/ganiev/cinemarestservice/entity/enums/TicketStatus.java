package com.ganiev.cinemarestservice.entity.enums;

public enum TicketStatus {
    STATUS_NEW,
    STATUS_PURCHASED,
    STATUS_REFUNDED
}
