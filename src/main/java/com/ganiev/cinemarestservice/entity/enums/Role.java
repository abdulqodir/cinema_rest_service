package com.ganiev.cinemarestservice.entity.enums;

public enum Role {

    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_CONTENT_MANAGER,
    ROLE_USER
}
