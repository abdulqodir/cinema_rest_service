package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.enums.TicketStatus;
import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class PurchaseHistory extends AbsEntity {

    @OneToOne
    Ticket ticket;

    @ManyToOne
    PayType payType;

}
    