package com.ganiev.cinemarestservice.entity;

import com.ganiev.cinemarestservice.entity.enums.AnnouncementStatus;
import com.ganiev.cinemarestservice.entity.template.AbsEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MovieAnnouncement extends AbsEntity {

    @OneToOne
    Movie movie;

    @Enumerated(value = EnumType.STRING)
    AnnouncementStatus status;

}
