package com.ganiev.cinemarestservice.service.seat;

import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface SeatService {
    HttpEntity<?> getAvailableSeats(UUID sessionId);
}
