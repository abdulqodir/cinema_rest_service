package com.ganiev.cinemarestservice.service.movie_announcement;

import com.ganiev.cinemarestservice.dto.movie_announcement.MovieAnnouncementDto;
import com.ganiev.cinemarestservice.entity.Movie;
import com.ganiev.cinemarestservice.entity.MovieAnnouncement;
import com.ganiev.cinemarestservice.entity.enums.AnnouncementStatus;
import com.ganiev.cinemarestservice.projection.announcement.AnnouncementView;
import com.ganiev.cinemarestservice.repository.MovieAnnouncementRepository;
import com.ganiev.cinemarestservice.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@RequiredArgsConstructor
public class MovieAnnouncementServiceImpl implements MovieAnnouncementService {

    private final MovieRepository movieRepo;
    private final MovieAnnouncementRepository movieAnnouncementRepo;

    @Override
    public HttpEntity<?> addNewAnnouncement(MovieAnnouncementDto dto) {
        Optional<Movie> optionalMovie = movieRepo.findById(dto.getMovieId());
        if (optionalMovie.isPresent()) {
            MovieAnnouncement movieAnnouncement = new MovieAnnouncement(
                    optionalMovie.get(),
                    AnnouncementStatus.valueOf(dto.getAnnouncementStatus())
            );

            movieAnnouncementRepo.save(movieAnnouncement);
            return ResponseEntity.ok(SUCCESS_SAVE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    @Override
    public HttpEntity<?> getAllAnnouncements() {
        List<AnnouncementView> allAnnouncements = movieAnnouncementRepo.getAllAnnouncements();
        return ResponseEntity.ok(allAnnouncements);
    }

    @Override
    public HttpEntity<?> deleteAnnouncement(UUID id) {
        Optional<MovieAnnouncement> optionalMovieAnnouncement = movieAnnouncementRepo.findById(id);
        if (optionalMovieAnnouncement.isPresent()) {
            movieAnnouncementRepo.delete(optionalMovieAnnouncement.get());
            return ResponseEntity.ok(SUCCESS_DELETE);
        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }
}
