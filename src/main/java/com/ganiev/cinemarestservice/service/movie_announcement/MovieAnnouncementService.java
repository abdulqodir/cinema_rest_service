package com.ganiev.cinemarestservice.service.movie_announcement;

import com.ganiev.cinemarestservice.dto.movie_announcement.MovieAnnouncementDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface MovieAnnouncementService {

    HttpEntity<?> addNewAnnouncement(MovieAnnouncementDto dto);
    HttpEntity<?> getAllAnnouncements();

    HttpEntity<?> deleteAnnouncement(UUID uuid);
}
