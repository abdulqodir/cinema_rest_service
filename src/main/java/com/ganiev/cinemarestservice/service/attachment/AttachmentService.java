package com.ganiev.cinemarestservice.service.attachment;

import com.ganiev.cinemarestservice.entity.Attachment;
import org.springframework.http.HttpEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface AttachmentService {
    public HttpEntity<?> saveAttachment(MultipartFile file);

    public HttpEntity<?> delete(UUID id);

    public HttpEntity<?> getAttachmentById(UUID id);
}
