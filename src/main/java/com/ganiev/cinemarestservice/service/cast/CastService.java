package com.ganiev.cinemarestservice.service.cast;

import com.ganiev.cinemarestservice.dto.cast.CastDto;
import org.springframework.http.HttpEntity;

import java.util.UUID;

public interface CastService {
    HttpEntity<?> saveCast(UUID id, CastDto dto);

    HttpEntity<?> getCastById(UUID id);

    HttpEntity<?> deleteCast(UUID uuid);

}
