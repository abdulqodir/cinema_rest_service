package com.ganiev.cinemarestservice.service.movie_session;

import com.ganiev.cinemarestservice.dto.movie_session.MovieSessionDto;
import com.ganiev.cinemarestservice.dto.movie_session.ReserveHallDto;
import com.ganiev.cinemarestservice.entity.MovieAnnouncement;
import com.ganiev.cinemarestservice.entity.MovieSession;
import com.ganiev.cinemarestservice.projection.movie_session.AnnouncementSessionView;
import com.ganiev.cinemarestservice.repository.HallRepository;
import com.ganiev.cinemarestservice.repository.MovieAnnouncementRepository;
import com.ganiev.cinemarestservice.repository.MovieSessionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@RequiredArgsConstructor
public class MovieSessionServiceImpl implements MovieSessionService {

    private final MovieAnnouncementRepository movieAnnouncementRepo;
    private final MovieSessionRepository movieSessionRepo;
    private final HallRepository hallRepo;

    @Override
    public HttpEntity<?> addNewSession(MovieSessionDto dto) {
        Optional<MovieAnnouncement> optionalAnnouncement = movieAnnouncementRepo.findById(dto.getAnnouncementId());
        if (optionalAnnouncement.isPresent()) {

            MovieAnnouncement movieAnnouncement = optionalAnnouncement.get();
            ArrayList<MovieSession> movieSessions = new ArrayList<>();
            Integer movieDuration = movieAnnouncement.getMovie().getDurationInMinute();

            for (ReserveHallDto reservedHall : dto.getReservedHalls()) {

                UUID hallId = reservedHall.getHallId();
                LocalDate startDate = LocalDate.parse(reservedHall.getStartDate());
                LocalDate endDate = null;

                if (reservedHall.getEndDate() != null) {
                    endDate = LocalDate.parse(reservedHall.getEndDate());
                }
                List<String> time = reservedHall.getTime();

                for (String t : time) {
                    LocalTime startTime = LocalTime.parse(t);
                    LocalTime endTime = LocalTime.parse(t).plusMinutes(movieDuration + ADDITIONAL_MINUTE);

                    if (endDate != null) {
                        long interval = endDate.toEpochDay() - startDate.toEpochDay();

                        for (long i = 0; i <= interval; i++) {
                            LocalDate date = startDate.plusDays(i);

                            if (checkAndCollectNewSessions(movieAnnouncement, movieSessions, hallId, startTime, endTime, date))
                                return ResponseEntity.status(HttpStatus.CONFLICT).body(EXISTS);
                        }

                    } else {
                        if (checkAndCollectNewSessions(movieAnnouncement, movieSessions, hallId, startTime, endTime, startDate))
                            return ResponseEntity.status(HttpStatus.CONFLICT).body(EXISTS);

                    }
                }
            }
            movieSessionRepo.saveAll(movieSessions);
            return ResponseEntity.ok(SUCCESS_SAVE);

        } else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);
    }

    private boolean checkAndCollectNewSessions(MovieAnnouncement movieAnnouncement, ArrayList<MovieSession> movieSessions, UUID hallId, LocalTime startTime, LocalTime endTime, LocalDate date) {
        Boolean timeRangeExists = movieSessionRepo.isTimeRangeExists(hallId, date, startTime, endTime);

        if (timeRangeExists)
            return true;

        MovieSession movieSession = new MovieSession(movieAnnouncement,
                hallRepo.getById(hallId),
                date,
                startTime,
                endTime,
                startTime.getHour() >= 16 ? ADDITIONAL_FEE_IN_PERCENT : 0.0
        );

        movieSessions.add(movieSession);
        return false;
    }

    @Override
    public HttpEntity<?> getAllMovieSessions() {

        return null;
    }

    @Override
    public HttpEntity<?> deleteSession(UUID id) {
        return null;
    }

    @Override
    public HttpEntity<?> getAnnouncementSessions(UUID announcementId) {

        AnnouncementSessionView announcementSession = movieSessionRepo.getAnnouncementSession(announcementId);
        if (announcementSession != null)
            return ResponseEntity.ok(announcementSession);
        else
            return ResponseEntity.status(HttpStatus.CONFLICT).body(OBJECT_NOT_FOUND);


    }
}
