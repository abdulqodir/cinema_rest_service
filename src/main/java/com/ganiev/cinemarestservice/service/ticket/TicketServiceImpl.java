package com.ganiev.cinemarestservice.service.ticket;

import com.ganiev.cinemarestservice.dto.ticket.TicketDto;
import com.ganiev.cinemarestservice.entity.MovieSession;
import com.ganiev.cinemarestservice.entity.Seat;
import com.ganiev.cinemarestservice.entity.Ticket;
import com.ganiev.cinemarestservice.entity.enums.TicketStatus;
import com.ganiev.cinemarestservice.repository.MovieSessionRepository;
import com.ganiev.cinemarestservice.repository.SeatRepository;
import com.ganiev.cinemarestservice.repository.TicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static com.ganiev.cinemarestservice.util.Constants.*;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepo;
    private final MovieSessionRepository movieSessionRepo;
    private final SeatRepository seatRepo;

    @Override
    public HttpEntity<?> generateNewTicket(TicketDto dto) {
        try {
            UUID seatId = dto.getSeatId();
            UUID sessionId = dto.getSessionId();

            MovieSession movieSession = movieSessionRepo.findById(sessionId)
                    .orElseThrow(() -> new ResourceAccessException(OBJECT_NOT_FOUND));

            Seat seat = seatRepo.findById(seatId)
                    .orElseThrow(() -> new ResourceAccessException(OBJECT_NOT_FOUND));

            Boolean ticketExists = ticketRepo.existsTicketBySeatAndMovieSession(seat, movieSession);

            if (ticketExists) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(EXISTS);

                // TODO: 3/28/2022 AGAR TICKET QAYTIB BERILGAN BULSA YANGI FOYDALANUVCHIGA

                // AGAR 30 MINUTDA BU XAM SOTIB ALMAS USER ID NULL BULISHI LOZIM
            } else {
                Double ticketPrice = ticketRepo.calculateTicketPrice(seat.getId(), movieSession.getId());

                // TODO: 3/28/2022 USER VA QR_CODE NI BERIB YUBORISH KERAK
                Ticket ticket = new Ticket(
                        movieSession,
                        seat,
                        null,
                        ticketPrice,
                        TicketStatus.STATUS_NEW,
                        null
                );

                ticketRepo.save(ticket);

                Timer timer = new Timer();
                TimerTask deleteTicket = new TimerTask() {
                    @Override
                    public void run() {
                        Ticket savedTicket = ticketRepo.findById(ticket.getId())
                                .orElseThrow(() -> new ResourceAccessException(OBJECT_NOT_FOUND));

                        if (savedTicket.getStatus().equals(TicketStatus.STATUS_NEW)) {
                            ticketRepo.delete(savedTicket);
                            System.out.println("TICKET DELETED");
                        }
                    }
                };
                timer.schedule(deleteTicket, 60000);

                return ResponseEntity.ok(SUCCESS_SAVE);
            }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }
}
