package com.ganiev.cinemarestservice.service.purchase;

import com.ganiev.cinemarestservice.dto.PurchaseDto;
import org.springframework.http.HttpEntity;

public interface PurchaseService {
    HttpEntity<?> purchaseTicket(PurchaseDto dto);
}
