package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.MovieAnnouncement;
import com.ganiev.cinemarestservice.projection.announcement.AnnouncementView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MovieAnnouncementRepository extends JpaRepository<MovieAnnouncement, UUID> {
    @Query(value = "select cast(ma.id as varchar) as movieAnnouncementId,\n" +
            "       m.title as title, \n" +
            "       cast(m.poster_img_id as varchar) as posterImgId,\n" +
            "       ma.status as status \n" +
            "from movie_announcement ma \n" +
            "         join movie m on m.id = ma.movie_id\n" +
            "where ma.status <> 'INACTIVE'",
            nativeQuery = true)
    List<AnnouncementView> getAllAnnouncements();
}
