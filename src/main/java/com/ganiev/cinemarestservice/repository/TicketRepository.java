package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.MovieSession;
import com.ganiev.cinemarestservice.entity.Role;
import com.ganiev.cinemarestservice.entity.Seat;
import com.ganiev.cinemarestservice.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface TicketRepository extends JpaRepository<Ticket, UUID> {

    @Query(nativeQuery = true,
            value = "select m.ticket_init_price +\n" +
                    "       (m.ticket_init_price * h.vip_additional_fee_in_percent / 100) +\n" +
                    "       (m.ticket_init_price * ms.additional_fee / 100) +\n" +
                    "       (m.ticket_init_price * pc.additional_fee_in_percent / 100) as price\n" +
                    "from movie_session ms\n" +
                    "         join movie_announcement ma on ma.id = ms.movie_announcement_id\n" +
                    "         join movie m on m.id = ma.movie_id\n" +
                    "         join hall h on h.id = ms.hall_id\n" +
                    "         join row r on h.id = r.hall_id\n" +
                    "         join seat s on r.id = s.row_id\n" +
                    "         join price_category pc on pc.id = s.price_category_id\n" +
                    "where ms.id = :sessionId\n" +
                    "  and s.id = :seatId")
    Double calculateTicketPrice(UUID seatId, UUID sessionId);

    Boolean existsTicketBySeatAndMovieSession(Seat seat, MovieSession movieSession);
}
