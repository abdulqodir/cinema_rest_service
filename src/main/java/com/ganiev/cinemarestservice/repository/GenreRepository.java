package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "genre", path = "genre")
public interface GenreRepository extends JpaRepository<Genre, UUID> {
}

