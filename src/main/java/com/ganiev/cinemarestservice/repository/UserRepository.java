package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

}
