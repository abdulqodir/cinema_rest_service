package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.RefundChargeFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "refund_charge_fee", path = "refund_charge_fee")
public interface RefundChargeFeeRepository extends JpaRepository<RefundChargeFee, UUID> {
}
