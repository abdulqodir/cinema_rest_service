package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.MovieSession;
import com.ganiev.cinemarestservice.projection.movie_session.AnnouncementSessionView;
import com.ganiev.cinemarestservice.projection.movie_session.SessionHallDateView;
import com.ganiev.cinemarestservice.projection.movie_session.SessionHallTimeView;
import com.ganiev.cinemarestservice.projection.movie_session.SessionTimeView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

public interface MovieSessionRepository extends JpaRepository<MovieSession, UUID> {

    @Query(value = "select exists(select *\n" +
            "              from movie_session\n" +
            "              where hall_id = :hallId \n" +
            "                  and date = :date \n" +
            "                  and :endTime >= start_time and :startTime <= end_time)",
            nativeQuery = true)
    Boolean isTimeRangeExists(UUID hallId, LocalDate date, LocalTime startTime, LocalTime endTime);

    @Query(nativeQuery = true,
            value = "select distinct date, " +
                    "cast(movie_announcement_id as varchar) as announcementId " +
                    "from movie_session " +
                    "where movie_announcement_id = :announcementId")
    List<SessionHallDateView> getSessionDates(UUID announcementId); // done


    @Query(nativeQuery = true,
            value = "select cast(m.id as varchar) as movieId, " +
                    "m.title as movieTitle, cast(ma.id as varchar) as announcementId " +
                    "from movie m " +
                    "join movie_announcement ma on m.id = ma.movie_id " +
                    "where ma.id = :announcementId")
    AnnouncementSessionView getAnnouncementSession(UUID announcementId); //done

    @Query(nativeQuery = true,
            value = "select distinct cast(h.id as varchar) as hallId, " +
                    "h.name as hallName, " +
                    "cast(ms.movie_announcement_id as varchar) announcementId, " +
                    "ms.date " +
                    "from movie_session ms " +
                    "join hall h on h.id = ms.hall_id " +
                    "where ms.date = :date and ms.movie_announcement_id = :announcementId")
    List<SessionHallTimeView> getSessionHalls(UUID announcementId, LocalDate date);

    @Query(nativeQuery = true,
            value = "select cast(ms.id as varchar) as sessionId, " +
                    "ms.start_time as time " +
                    " from movie_session ms " +
                    "join movie_announcement ma on ma.id = ms.movie_announcement_id " +
                    "join hall h on h.id = ms.hall_id " +
                    "where movie_announcement_id = :announcementId " +
                    "and ms.date = :date and h.id = :hallId")
    List<SessionTimeView> getSessionTimes(UUID announcementId, LocalDate date, UUID hallId);
}
