package com.ganiev.cinemarestservice.repository;

import com.ganiev.cinemarestservice.entity.Movie;
import com.ganiev.cinemarestservice.projection.movie.MovieView;
import com.ganiev.cinemarestservice.projection.movie.SingleMovieView;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MovieRepository extends JpaRepository<Movie, UUID> {

    @Query(value = "select cast(m.id as varchar)            as id,\n" +
            "       m.title,\n" +
            "       m.release_date                   as releaseDate,\n" +
            "       cast(m.poster_img_id as varchar) as posterImgId\n" +
            "from movie m\n" +
            "where lower(title) like lower(concat('%', :search, '%'))\n", nativeQuery = true)
    List<MovieView> findAllMovies(Pageable pageable, String search);

    // TODO: 3/23/2022  No Dialect mapping for JDBC type digan exception otyapti
    @Query(value = "select cast(id as varchar) as id," +
            "title, description," +
            "duration_in_minute as durationInMinute, " +
            "trailer_video_url as trailerVideoUrl," +
            "poster_img_id as posterImgId," +
            "release_date as realiseDate," +
            "budget, " +
            "cast(distributor_id as varchar) as distributorId from movie", nativeQuery = true)
    Optional<SingleMovieView> getSingleMovie(UUID id);
}
