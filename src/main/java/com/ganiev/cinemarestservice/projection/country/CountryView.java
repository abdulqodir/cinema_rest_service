package com.ganiev.cinemarestservice.projection.country;

import java.util.UUID;

public interface CountryView {
    UUID getId();

    String getName();
}
