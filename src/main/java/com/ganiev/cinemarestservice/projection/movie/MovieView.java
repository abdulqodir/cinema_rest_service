package com.ganiev.cinemarestservice.projection.movie;

import java.time.LocalDate;
import java.util.UUID;

public interface MovieView {
    UUID getId();

    String getTitle();

    UUID getPosterImgId();

    LocalDate getReleaseDate();
}
