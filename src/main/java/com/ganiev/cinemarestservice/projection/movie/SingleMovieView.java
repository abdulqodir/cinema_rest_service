package com.ganiev.cinemarestservice.projection.movie;

import com.ganiev.cinemarestservice.projection.cast.CastView;
import com.ganiev.cinemarestservice.projection.country.CountryView;
import com.ganiev.cinemarestservice.projection.genre.GenreView;
import com.ganiev.cinemarestservice.projection.distributor.DistributorView;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface SingleMovieView {
    UUID getId();

    String getTitle();

    String getDescription();

    Integer getDurationInMinute();

    String getTrailerVideoUrl();

    UUID getPosterImgId();

    String  getRealiseDate();

    Double getBudget();

    UUID getDistributorId();

    UUID getDistributorName();

//    List<CastView> getCasts();
//
//    List<GenreView> getGenres();
//
//    List<CountryView> getCountries();

}
