package com.ganiev.cinemarestservice.projection.distributor;

import com.ganiev.cinemarestservice.entity.Distributor;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(types = Distributor.class)
public interface DistributorView {
    UUID getId();

    String getName();

    String getDescription();

    UUID getLogoId();
}
