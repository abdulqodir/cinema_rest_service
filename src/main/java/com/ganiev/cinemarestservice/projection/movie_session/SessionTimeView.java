package com.ganiev.cinemarestservice.projection.movie_session;

import java.time.LocalTime;
import java.util.UUID;

public interface SessionTimeView {

    LocalTime getTime();

    UUID getSessionId();
}
