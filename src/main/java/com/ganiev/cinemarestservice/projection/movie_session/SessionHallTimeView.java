package com.ganiev.cinemarestservice.projection.movie_session;

import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.UUID;

public interface SessionHallTimeView {

    UUID getHallId();

    String getHallName();

    @Value("#{@movieSessionRepository.getSessionTimes(target.announcementId, target.date, target.hallId)}")
    List<SessionTimeView> getSessionTimes();


}
