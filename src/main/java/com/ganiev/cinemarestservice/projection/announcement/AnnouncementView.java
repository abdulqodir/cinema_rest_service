package com.ganiev.cinemarestservice.projection.announcement;

import java.util.UUID;

public interface AnnouncementView {
    UUID getMovieAnnouncementId();

    String getTitle();

    UUID getPosterImgId();

    String getStatus();
}
