package com.ganiev.cinemarestservice.projection.price_category;

import com.ganiev.cinemarestservice.entity.PriceCategory;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "priceCategoryProjection", types = {PriceCategory.class})
public interface PriceCategoryView {
    UUID getId();

    String getName();

    Double getAdditionalFeeInPercent();

    String getColor();
}
