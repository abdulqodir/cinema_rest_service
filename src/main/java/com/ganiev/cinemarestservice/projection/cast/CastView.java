package com.ganiev.cinemarestservice.projection.cast;

import java.util.UUID;

public interface CastView {
    UUID getId();

    String getFullName();

    String getPhotoId();
}
