package com.ganiev.cinemarestservice.common;

import com.ganiev.cinemarestservice.entity.*;
import com.ganiev.cinemarestservice.entity.enums.AnnouncementStatus;
import com.ganiev.cinemarestservice.entity.enums.CastType;
import com.ganiev.cinemarestservice.entity.enums.TicketStatus;
import com.ganiev.cinemarestservice.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    @Value("${spring.sql.init.mode}")
    String initMode;

    private final HallRepository hallRepository;
    private final MovieRepository movieRepository;
    private final CastRepository castRepository;
    private final CountryRepository countryRepository;
    private final GenreRepository genreRepository;
    private final MovieAnnouncementRepository movieAnnouncementRepo;
    private final MovieSessionRepository movieSessionRepo;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            // SAVING HALL WITH ROWS, SEATS AND PRICE_CATEGORIES

            PriceCategory priceCategory1 = new PriceCategory(
                    "CATEGORY 1",
                    1.5,
                    "#AAAAAA");

            List<Seat> seats1 = new ArrayList<>();
            List<Seat> seats2 = new ArrayList<>();
            List<Seat> seats3 = new ArrayList<>();
            List<Seat> seats4 = new ArrayList<>();
            List<Seat> seats5 = new ArrayList<>();

            for (int i = 1; i <= 20; i++) {
                seats1.add(new Seat(i, priceCategory1));
                seats2.add(new Seat(i, priceCategory1));
                seats3.add(new Seat(i, priceCategory1));
                seats4.add(new Seat(i, priceCategory1));
                seats5.add(new Seat(i, priceCategory1));
            }

            ArrayList<Row> rows = new ArrayList<>(Arrays.asList(
                    new Row(1, seats1),
                    new Row(2, seats2),
                    new Row(3, seats3),
                    new Row(4, seats4),
                    new Row(5, seats5)
            ));

            Hall hall = new Hall("Hall 1", 0.0, rows);

            hallRepository.save(hall);


            // SAVING CASTS

            ArrayList<Cast> casts = new ArrayList<>(
                    Arrays.asList(
                            new Cast("Robert Downey, Jr.", CastType.CAST_ACTOR, null),
                            new Cast("Stanley Kubrick", CastType.CAST_DIRECTOR, null),
                            new Cast("Scarlett Johansson", CastType.CAST_ACTRESS, null)
                    )
            );

            castRepository.saveAll(casts);

            // SAVING COUNTRIES

            ArrayList<Country> countries = new ArrayList<>(
                    Arrays.asList(
                            new Country("USA"),
                            new Country("Germany"),
                            new Country("Russia"),
                            new Country("India"),
                            new Country("Uzbekistan")
                    )
            );

            countryRepository.saveAll(countries);

            // SAVING GENRES

            ArrayList<Genre> genres = new ArrayList<>(
                    Arrays.asList(
                            new Genre("Horror"),
                            new Genre("Drama"),
                            new Genre("Action"),
                            new Genre("Comedy"),
                            new Genre("Adventure")
                    )
            );

            genreRepository.saveAll(genres);

            // SAVING MOVIES

            Distributor distributor = new Distributor("Marvel", "Description", null);

            ArrayList<Movie> movies = new ArrayList<>(
                    Arrays.asList(
                            new Movie(
                                    "Spider-Man: No Way Home",
                                    "Peter Parker's secret identity is revealed to the entire world. Desperate for help, Peter turns to Doctor Strange to make the world forget that he is Spider-Man. The spell goes horribly wrong and shatters the multiverse, bringing in monstrous villains that could destroy the world.",
                                    148,
                                    40000.0,
                                    "https://www.youtube.com/watch?v=JfVOs4VSpmA",
                                    null,
                                    LocalDate.parse("2021-12-17"),
                                    200_000_000.0,
                                    5.0,
                                    distributor,
                                    null,
                                    null,
                                    null
                            ),
                            new Movie(
                                    "Doctor strange in the multiverse of madness",
                                    "Dr. Stephen Strange casts a forbidden spell that opens the door to the multiverse, including an alternate version of himself, whose threat to humanity is too great for the combined forces of Strange, Wong, and Wanda Maximoff.",
                                    120,
                                    50000.0,
                                    "https://www.youtube.com/watch?v=Rt_UqUm38BI",
                                    null,
                                    LocalDate.parse("2022-05-06"),
                                    300_000_000.0,
                                    5.0,
                                    distributor,
                                    null,
                                    null,
                                    null
                            ),
                            new Movie(
                                    "Thor love and thunder ",
                                    "Thor: Love and Thunder is about to show how far the Norse God has come in the Marvel Cinematic Universe (MCU). After two less-than-impressive solo movies – the less said about The Dark World, the better – Thor thundered his way back into our good books in Taiki Waititi's Thor: Ragnarok.",
                                    133,
                                    45000.0,
                                    "https://www.youtube.com/watch?v=G2CnIRC4R18", null,
                                    LocalDate.parse("2022-07-08"),
                                    250_000_000.0,
                                    5.0,
                                    distributor,
                                    null,
                                    null,
                                    null
                            )
                    )
            );

            movieRepository.saveAll(movies);

            ArrayList<MovieAnnouncement> movieAnnouncements = new ArrayList<>(
                    Arrays.asList(
                            new MovieAnnouncement(movies.get(0), AnnouncementStatus.ACTIVE),
                            new MovieAnnouncement(movies.get(1), AnnouncementStatus.ACTIVE),
                            new MovieAnnouncement(movies.get(2), AnnouncementStatus.ACTIVE)
                    )
            );

            movieAnnouncementRepo.saveAll(movieAnnouncements);


            ArrayList<MovieSession> movieSessions = new ArrayList<>(
                    Arrays.asList(
                            new MovieSession(
                                    movieAnnouncements.get(0),
                                    hall,
                                    LocalDate.now().plusDays(5),
                                    LocalTime.of(9, 0),
                                    LocalTime.of(10, 30),
                                    0.0
                            ),
                            new MovieSession(
                                    movieAnnouncements.get(0),
                                    hall,
                                    LocalDate.now().plusDays(5),
                                    LocalTime.of(18, 0),
                                    LocalTime.of(20, 30),
                                    2.0
                            )
                    )
            );

            movieSessionRepo.saveAll(movieSessions);

            ArrayList<Role> roles = new ArrayList<>(Arrays.asList(
                    new Role(com.ganiev.cinemarestservice.entity.enums.Role.ROLE_ADMIN),
                    new Role(com.ganiev.cinemarestservice.entity.enums.Role.ROLE_USER),
                    new Role(com.ganiev.cinemarestservice.entity.enums.Role.ROLE_CONTENT_MANAGER),
                    new Role(com.ganiev.cinemarestservice.entity.enums.Role.ROLE_SUPER_ADMIN)
            ));

            roleRepository.saveAll(roles);

            User user = new User(
                    "Abdulqodir",
                    "Ganiev",
                    "ganiev",
                    "123",
                    "ganiev@gmail.com",
                    "+998901570711",
                    roles
            );

            userRepository.save(user);

            ArrayList<Ticket> tickets = new ArrayList<>(Arrays.asList(
                    new Ticket(
                            movieSessions.get(0),
                            seats1.get(1),
                            null,
                            50000.0,
                            TicketStatus.STATUS_NEW,
                            user
                    ),
                    new Ticket(
                            movieSessions.get(0),
                            seats1.get(2),
                            null,
                            50000.0,
                            TicketStatus.STATUS_REFUNDED,
                            user
                    ),
                    new Ticket(
                            movieSessions.get(0),
                            seats1.get(3),
                            null,
                            50000.0,
                            TicketStatus.STATUS_PURCHASED,
                            user
                    )
            ));

            ticketRepository.saveAll(tickets);
        }
    }
}
