package com.ganiev.cinemarestservice.dto.hall;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class HallDto {

    String name;

    Double vipAdditionalFeeInPercent;

    Integer numberOfRows;

}
