package com.ganiev.cinemarestservice.dto.row;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class RowDto {

    UUID id;

    Integer seatNumFrom;

    Integer seatNumTo;

    UUID priceCategoryId;

}
