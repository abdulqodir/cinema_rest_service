package com.ganiev.cinemarestservice.dto.seat;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class SeatDto {
    UUID rowId;

}
