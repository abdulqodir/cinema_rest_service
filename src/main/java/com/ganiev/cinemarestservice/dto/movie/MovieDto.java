package com.ganiev.cinemarestservice.dto.movie;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MovieDto {

    String title;

    String description;

    Integer durationInMinute;

    Double ticketInitPrice;

    String trailerVideoUrl;

    String realiseDate;

    Double budget;

    Double distributorShareInPercentage;

    UUID distributorId;

    List<UUID> castsId;

    List<UUID> genresId;

    List<UUID> countriesId;
}
